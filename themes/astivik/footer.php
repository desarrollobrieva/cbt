</section> <!-- content-end -->
<footer>
<?php $currentlang = get_bloginfo('language'); ?>
<?php if($currentlang=="es-ES"):?>
	<?php  
		$about = 'Acerca de astivik';
		$service = 'Servicios';
		$taller = 'Taller'; 
		$contact = 'Contacto y Soporte';
		$project = 'Proyectos';
	?>

	<!-- end español -->
<?php else: ?>
	<?php  
		$about = 'About Astivik';
		$service = 'Services';
		$taller = 'Workshop';
		$contact = 'Contact and Support';
		$project = 'Projects'
	?>
<?php endif; ?>
	<?php $currentlang = get_bloginfo('language'); ?>
	<div class="container footer">
		<div class="row">
			<div class="content-logo";>
				

				<div class="img-footer-astivik"><img data-srcset="<?php echo get_template_directory_uri();?>/assets/img/logo_astivik_footer.svg" alt="Logotipo Astivik" title="" class="lazy img-responsive"/></div>
				<div class="separacion"></div>
				<div class="img-footer-certification " ><img data-srcset="<?php echo get_template_directory_uri();?>/assets/img/alllogos.png" alt="certificaciones de astivik" title="" class="lazy img-responsive"/></div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="linea"></div>
		</div>
		<style>
		@media (min-width: 0px) and (max-width: 767px) {
			.ocultar{
				display:none;
			}
		}
	</style>
	<div class="row ocultar">
		<div class="col-md-2 col-sm-auto wow">
				<h4><?php echo $about; ?></h4>
			<?php if($currentlang=="es-ES"):?>
				<!-- español-->
				<ul>
					<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#quienes-somos'; ?>">Quiénes somos</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#mision'; ?>">Proyección y misión</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#valores'; ?>">Valores de Astivik</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/index.php/seguridad/'; ?>">Control y medio ambiente</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/index.php/seguridad/'; ?>">Seguridad y responsabilidad</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/politica-para-el-tratamiento-de-datos-personales'; ?>">Políticas para el tratamiento de datos personales y cookies</a></li>
					
				</ul>
				<!-- end español -->
			<?php else: ?>
				<!-- english -->
				
				<ul>
					<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#quienes-somos'; ?>">About Us</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#mision'; ?>">Mission and Vision</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#valores'; ?>">Our Values</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/index.php/seguridad/'; ?>">Control and Environment</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/index.php/seguridad/'; ?>">Safety and Responsibility</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/politica-para-el-tratamiento-de-datos-personales'; ?>">Policies and cookies</a></li>
					
				</ul>
				<!-- end English -->
			<?php endif; ?>

		</div>
		<div class="col-md-2 col-sm-auto wow">
			<h4><?php echo $service; ?></h4>
			<ul>
				<?php $categories = get_categories( array('taxonomy' => 'Servicios','hide_empty' => false,'orderby' => 'name','order' => 'DES'));?>
				<?php foreach ($categories as $category):?>
					<li><a href="<?php echo get_term_link( $category->term_id ); ?>"><?php echo $category->name; ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="col-md-2 col-sm-auto wow">
			<h4><?php echo $taller.' Astivik'; ?></h4>
			<ul>
				<?php $the_query = new WP_Query('post_type=capacidades&order=ASC'); ?>
				<?php if ($the_query -> have_posts()) : while( $the_query -> have_posts() ) : $the_query -> the_post(); ?>

					<li><a href="<?php the_permalink();?>"><?php the_title(); ?></a></li>
				<?php endwhile; endif; ?>
				<?php if($currentlang=="es-ES"):?>
					<!-- español-->
					<li><a href="<?php echo bloginfo('url').'/index.php/mapa-infraestructura/'; ?>">Mapa de Infraestructura</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab3'; ?>">Programar cita</a></li>
					<!-- end español -->
				<?php else: ?>
					<!-- english -->
					<li><a href="<?php echo bloginfo('url').'/index.php/mapa-infraestructura/'; ?>">Map of Infrastructure</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab3'; ?>">Schedule Appointment</a></li>
					<!-- end English -->
				<?php endif; ?>
			</ul>
		</div>

			<!-- español-->
			<div class="col-md-3 col-sm-auto wow">
				<h4><?php echo $contact; ?></h4>
		<?php if($currentlang=="es-ES"):?>
				<ul>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab5'; ?>" title="">Directorio Corporativo</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab2'; ?>" title="">Trabaje con Nosotros</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab4'; ?>" title="">Proveedores</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab1'; ?>" title="">Cotizar Servicios</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab7'; ?>" title="">PQRS</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab3'; ?>" title="">Hablar con un asesor</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab6'; ?>" title="">Preguntas frecuentes</a></li>
				</ul>
			</div>
			<div class="col-md-2 col-sm-auto wow">
				<ul>
					<li class="color-negro"><a style="font-size: 15px;

					color: #000;
					font-family: 'Ubuntu', sans-serif;
					font-weight: bold;" href="<?php echo bloginfo('url').'/index.php/proyecto/'; ?>">Proyectos Realizados</a></li>
					<li class="color-negro"><a style="font-size: 15px;

					color: #000;
					font-family: 'Ubuntu', sans-serif;
					font-weight: bold;" href="<?php echo bloginfo('url').'/index.php/noticias'; ?>">Noticias y Eventos</a></li>

					<div class="icons-social">
						<p>Astivik Social</p>
						<ul style="display: inline-flex;">

							<!--<li> <a href="https://www.facebook.com/ASTIVIK"> <i class="fa fa-facebook fa-2x "></i></a>  </li>-->
							<li> <a href="https://www.instagram.com/astivikshipyard/" target="_blank"> <i class="fa fa-instagram fa-2x "></i></a> </li>
							<li><a href="https://www.linkedin.com/company/industrias-astivik/" target="_blank"> <i class="fa fa-linkedin fa-2x "></i></a></span></li>
							<li><a href="https://www.youtube.com/user/Astivik1" target="_blank"> <i class="fa fa-youtube-play fa-2x "></i></a></span></li>
							

						</ul>
					</div>
				</ul>
			</div>
			<!-- end español -->
		<?php else: ?>
			<!-- english -->
			
				<ul>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab5'; ?>" title="">Corporative Directory</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab2'; ?>" title="">Work with us</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab4'; ?>" title="">Suppliers</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab1'; ?>" title="">Service Quote</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab7'; ?>" title="">Quality Survey</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab3'; ?>" title="">Talk to an agent</a></li>
					<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab6'; ?>" title="">FAQ</a></li>
				</ul>
			</div>
			<div class="col-md-2 col-sm-auto wow">
				<ul>
					<li class="color-negro"><a style="font-size: 15px;

					color: #000;
					font-family: 'Ubuntu', sans-serif;
					font-weight: bold;" href="<?php echo bloginfo('url').'/index.php/proyecto/'; ?>">Successful Projects</a></li>
					<li class="color-negro"><a style="font-size: 15px;

					color: #000;
					font-family: 'Ubuntu', sans-serif;
					font-weight: bold;" href="<?php echo bloginfo('url').'/index.php/noticias'; ?>">News and Events</a></li>

					<div class="icons-social">
						<p>Astivik Social</p>
						<ul style="display: inline-flex;">
							<!--	<li> <a href=""> <i class="fa fa-facebook fa-2x "></i></a>  </li>-->
							<li> <a href="https://www.instagram.com/astivikshipyard/" target="_blank"> <i class="fa fa-instagram fa-2x "></i></a> </li>
							<li><a href="https://www.linkedin.com/company/industrias-astivik/" target="_blank"> <i class="fa fa-linkedin fa-2x "></i></a></span></li>
							<li><a  target="_blank" href="https://www.youtube.com/user/Astivik1"> <i class="fa fa-youtube-play fa-2x "></i></a></span></li>

						</ul>
					</div>
				</ul>
			</div>
			<!-- end English -->
		<?php endif; ?>
	</div>

</div>


<!---*******************************NUEVO FOOTER MENU****************************************-->

<div class="panel-group footer d-lg-none d-md-none" id="accordion-footer">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title">
				<h4 style="border-bottom: 1px solid #0000001a;padding: 12px;" class="accordion-toggle item-links" data-toggle="collapse" data-parent="#accordion-footer" href="#panel1"><?php echo $about; ?><span class="separator-arrow"><i class="fa fa-caret-down fa-1x"></i></span></h4>
			</div>
		</div>
		<div id="panel1" class="panel-collapse collapse">
			<div class="panel-body">
				<div class="col-md-2 col-sm-auto wow" id="menu_footer">
					<?php if($currentlang=="es-ES"):?>
						<!-- español-->
						<ul>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#quienes-somos'; ?>">Quiénes somos</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#mision'; ?>">Proyección y misión</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#valores'; ?>">Valores de Astivik</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/seguridad/'; ?>">Control y medio ambiente</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/seguridad/'; ?>">Seguridad y responsabilidad</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/'; ?>">Politicas y cookies</a></li>
							
						</ul>
						<!-- end español -->
					<?php else: ?>
						<!-- english -->

						<ul>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#quienes-somos'; ?>">About Us</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#mision'; ?>">Mission and Vision</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/about/#valores'; ?>">Our Values</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/seguridad/'; ?>">Control and Environment</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/seguridad/'; ?>">Safety and Responsibility</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/index.php/'; ?>">Policies and cookies</a></li>
							
						</ul>
						<!-- end English -->
					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title">
				<h4 style="border-bottom: 1px solid #0000001a;padding: 12px;" class="accordion-toggle item-links" data-toggle="collapse" data-parent="#accordion-footer" href="#panel2"><?php echo $service; ?><span class="separator-arrow" ><i class="fa fa-caret-down fa-1x"></i></span></h4>
			</div>
		</div>
		<div id="panel2" class="panel-collapse collapse">
			<div class="panel-body">
				<div class="col-md-2 col-sm-auto wow">

					<ul>
						<?php $categories = get_categories( array('taxonomy' => 'Servicios','hide_empty' => false,'orderby' => 'name','order' => 'DES'));?>
						<?php foreach ($categories as $category):?>
							<li><a href="<?php echo get_term_link( $category->term_id ); ?>"><?php echo $category->name; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="panel-title">
					<h4 style="border-bottom: 1px solid #0000001a; padding: 12px;" class="accordion-toggle item-links" data-toggle="collapse" data-parent="#accordion-footer" href="#panel3"><?php echo $taller.' Astivik'; ?><span class="separator-arrow" ><i class="fa fa-caret-down fa-1x"></i></span></h4>
				</div>
			</div>
			<div id="panel3" class="panel-collapse collapse">
				<div class="panel-body">
					<div class="col-md-2 col-sm-auto wow">

						<ul>
							<?php $the_query = new WP_Query('post_type=capacidades&order=ASC'); ?>
							<?php if ($the_query -> have_posts()) : while( $the_query -> have_posts() ) : $the_query -> the_post(); ?>

								<li><a href="<?php the_permalink();?>"><?php the_title(); ?></a></li>
							<?php endwhile; endif; ?>

							<?php if($currentlang=="es-ES"):?>
								<!-- español-->
								<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab3'; ?>">Programar cita</a></li>
								<!-- end español -->
							<?php else: ?>
								<!-- english -->
								<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab3'; ?>">Schedule Appointment</a></li>
								<!-- end English -->
							<?php endif; ?>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title">
				<h4 style="border-bottom: 1px solid #0000001a;padding: 12px;" class="accordion-toggle item-links" data-toggle="collapse" data-parent="#accordion-footer" href="#panel4"><?php echo $contact; ?><span class="separator-arrow" ><i class="fa fa-caret-down fa-1x"></i></span></h4>
			</div>
		</div>
		<div id="panel4" class="panel-collapse collapse">
			<div class="panel-body">
				<div class="col-md-3 col-sm-auto wow">
					<ul>
						<?php if($currentlang=="es-ES"):?>
							<!-- español-->
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab5'; ?>" title="">Directorio Corporativo</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab2'; ?>" title="">Trabaje con Nosotros</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab4'; ?>" title="">Proveedores</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/#tab1'; ?>" title="">Cotizar Servicios</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab7'; ?>" title="">PQRS</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/#tab3'; ?>" title="">Hablar con un asesor</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab6'; ?>" title="">Preguntas frecuentes</a></li>
							<!-- end español -->
						<?php else: ?>
							<!-- english -->
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab5'; ?>" title="">Corporate Directory</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab2'; ?>" title="">Work With Us</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab4'; ?>" title="">Suppliers</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/#tab1'; ?>" title="">Quote Service</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab7'; ?>" title="">Quality Survey</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/#tab3'; ?>" title="">Talk to an agent</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab6'; ?>" title="">FAQ</a></li>
							<!-- end English -->
						<?php endif; ?>
					</ul>
				</div>
				<!-- end English -->

			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-title">
				<h4 style="border-bottom: 1px solid #0000001a;padding: 12px;font-size: 15px;color: #000;font-family: 'Ubuntu', sans-serif;font-weight: bold;" class="accordion-toggle item-links" data-toggle="collapse" data-parent="#accordion-footer" href="#panel6"><?php echo $project; ?><span class="separator-arrow" ><i class="fa fa-caret-down fa-1x"></i></span></h4>
			</div>
		</div>
		<div id="panel6" class="panel-collapse collapse">
			<div class="panel-body">
				<div class="col-md-2 col-sm-auto wow">
					<ul>
						<?php if($currentlang=="es-ES"):?>
							<!-- Español-->
							<li><a href="<?php echo bloginfo('url').'/index.php/proyecto'; ?>" title="">Proyectos Realizados</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/noticias'; ?>" title="">Noticias y Evento</a></li>
							
							<!-- End Español -->
						<?php else: ?>
							<!-- English -->
							<li><a href="<?php echo bloginfo('url').'/index.php/proyecto'; ?>" title="">Successful Projects</a></li>
							<li><a href="<?php echo bloginfo('url').'/index.php/noticias'; ?>" title="">News and Events</a></li>
							<!-- End English -->
						<?php endif; ?>
						
					</ul>
				</div>

			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>

<style type="text/css">
.separator-arrow{margin: 0 4px;}
</style>

<script type="text/javascript">
$(document).ready(function(){$(".item-links").click(function(){$(this).find("i").hasClass("fa-caret-down")?$(this).find("i").removeClass("fa-caret-down").addClass("fa-caret-up"):$(this).find("i").removeClass("fa-caret-up").addClass("fa-caret-down")})});
</script>


<!---*****************************************************************************-->

<div class="container-fluid ">
	<div class="row">
		<div style="width: 100%">
			<div class="copy">
				<?php if($currentlang=="es-ES"):?>
					<!-- español-->
					<p> Copyright Astivik 2018 | Diseño de sitio web inteligente por: <a href="https://grupobrieva.com"> www.grupobrieva.com 2018 </a> | Desarrollo <a href="https://slicegroup.xyz"> www.slicegroup.xyz</a> | Fuentes de Google Fonts | Diseño en: Bootstrap </p>
					<!-- end español -->
				<?php else: ?>
					<!-- english -->
					<p> Copyright Astivik 2018 | Intelligent website design by: <a href="https://grupobrieva.com"> www.grupobrieva.com 2018 </a> | Developed by <a href="https://slicegroup.xyz"> www.slicegroup.xyz</a> | Google Fonts | Designed with: Bootstrap </p>
					<!-- end English -->
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<!---*****************************************************************************-->
<script>
$(".openpanel").on("click",function(){$("#panel6").collapse("show")}),$(".closepanel").on("click",function(){$("#panel6").collapse("hide")}),$("#accordion-footer").on("show.bs.collapse",function(){$("#accordion-footer .in").collapse("hide")});
</script>
<!---*****************************************************************************-->





</footer>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


<?php wp_footer(); ?> <!-- funcion de footer para traer los cambios en el footer -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/8.14.0/lazyload.min.js"></script>
<script>
var myLazyLoad=new LazyLoad({elements_selector:".lazy"});
var url = window.location.toString();
if(url.indexOf('?lang=en') == -1){
	$("#field_qh4icy3").attr("placeholder","Nombre Completo"),$("#frm_field_11_container label").text("Nombre Completo *"),$("#frm_field_17_container label").text("Empresa *"),$("#field_3limd").attr("placeholder","Empresa"),$("#frm_field_115_container label").text("País"),$("#frm_field_20_container label").text("Teléfono *"),$("#field_wva1i").attr("placeholder","Teléfono"),$("#frm_field_114_container label").text("Servicio a Cotizar *"),$("#frm_field_22_container label").text("Nombre de la Embarcación *"),$("#field_numvi").attr("placeholder","Nombre de la Embarcación"),$("#frm_field_23_container label").text("Tipo de Embarcación *"),$("#frm_field_24_container label").text("Nro IMO *"),$("#field_4wg7j").attr("placeholder","Nro IMO"),$("#frm_field_25_container label").text("Eslora *"),$("#field_3jwea").attr("placeholder","Eslora"),$("#frm_field_26_container label").text("Manga *"),$("#field_wm5o8").attr("placeholder","Manga"),$("#frm_field_27_container label").text("calado *"),$("#field_3du73").attr("placeholder","Calado"),$("#frm_field_28_container label").text("Peso en Rosca *"),$("#field_hiwh4").attr("placeholder","Peso en Rosca"),$("#frm_field_110_container label").text("Fecha estimada de dique *"),$("#frm_field_33_container label").text("Descripción breve de los servicios *"),$("#field_hhqxe").attr("placeholder","Descripción breve de los servicios"),$("#frm_field_120_container a").text("He leído y acepto las políticas de privacidad y tratamiento de datos"),$("#frm_field_91_container label").text("Nombre*"),$("#field_mqm2h").attr("placeholder","Nombre"),$("#frm_field_92_container label").text("Apellido *"),$("#field_14shb").attr("placeholder","Apellido"),$("#frm_field_93_container label").text("Teléfono *"),$("#field_cfgfw").attr("placeholder","Teléfono"),$("#frm_field_94_container label").text("Posición a la que se postula *"),$("#frm_field_95_container label").text("Adjunte su hoja de vida *"),$("#frm_field_105_container label").text("Nombre*"),$("#field_f94mz").attr("placeholder","Nombre"),$("#frm_field_106_container label").text("Apellido*"),$("#field_gfc1").attr("placeholder","Apellido"),$("#frm_field_107_container label").text("Email *"),$("#frm_field_108_container label").text("Mensaje *"),$("#field_zxqqa").attr("placeholder","Mensaje"),$(".frm_radio:nth-child(1)").empty(),$(".frm_radio:nth-child(1)").append('<label for="field_3tjoo-0"><input type="radio" name="item_meta[98]" id="field_3tjoo-0" value="Queja" data-frmval="Sugerencia" data-reqmsg="Este campo no puede estar en blanco." data-invmsg="PQRS is invalid" class="form-control"> Queja</label>'),$(".frm_radio:nth-child(2)").empty(),$(".frm_radio:nth-child(2)").append('<label for="field_3tjoo-1"><input type="radio" name="item_meta[98]" id="field_3tjoo-1" value="Reclamo" data-frmval="Sugerencia" data-reqmsg="Este campo no puede estar en blanco." data-invmsg="PQRS is invalid" class="form-control"> reclamo</label>'),$(".frm_radio:nth-child(3)").empty(),$(".frm_radio:nth-child(3)").append('<label for="field_3tjoo-2"><input type="radio" name="item_meta[98]" id="field_3tjoo-2" value="Sugerencia" checked="checked" data-frmval="Sugerencia" data-reqmsg="Este campo no puede estar en blanco." data-invmsg="PQRS is invalid" class="form-control"> Sugerencia</label>'),$("#frm_field_99_container label").text("Nombre *"),$("#field_mpx4f").attr("placeholder","Nombre"),$("#frm_field_100_container label").text("Empresa *"),$("#field_tcnaz").attr("placeholder","Empresa"),$("#frm_field_125_container label").text("País *"),$("#frm_field_102_container label").text("Teléfono *"),$("#field_2zcnh").attr("placeholder","Teléfono"),$("#frm_field_103_container label").text("Breve explicación de la situación *"),$("#field_wxnv5").attr("placeholder","Breve explicación de la situación"),$("#frm_field_121_container label").text("Mensaje *"),$("#field_2cujp").attr("placeholder","Mensaje"),$(".frm_message p").text("Su solicitud ha sido enviada y recibida por el equipo de Astivik Shipyard. Estaremos canalizando su solicitud con nuestro equipo comercial.");
}else{
	$('.frm_submit button').text('Save');
}
</script>
</body>
</html>
