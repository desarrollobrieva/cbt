<?php
if ( ! defined( 'ABSPATH' ) ) { exit; }


// Definimos la función
function ss_scripts() {
// El primer paso es usar wp_register_script para registrar el script que queremos cargar. Fíjense que aquí sí usamos *get_template_directory_uri()*
wp_register_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array( 'jquery'), '1.0.0', true );
wp_register_script( 'primer-script', get_template_directory_uri() . '/assets/js/menu0.js', array( 'jquery'), '1.0.0', true );
wp_register_script( 'segundo-script', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery'), '1.0.0', true );
wp_register_script( 'tercer-script', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery'), '1.0.0', true );
	
// Una vez que registramos el script debemos colocarlo en la cola de WordPress
wp_enqueue_script( 'tercer-script' );
wp_enqueue_script( 'jquery' );
wp_enqueue_script( 'primer-script' );
wp_enqueue_script( 'segundo-script' );

}

// Agregamos la función a la lista de cargas de WordPress.
add_action( 'wp_enqueue_scripts', 'ss_scripts' );


// Set up theme support
function elementor_hello_theme_setup() {

	add_theme_support( 'menus' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
	add_theme_support( 'custom-logo', array(
		'height' => 70,
		'width' => 350,
		'flex-height' => true,
		'flex-width' => true,
	) );

	add_theme_support( 'woocommerce' );

	load_theme_textdomain( 'elementor-hello-theme', get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'elementor_hello_theme_setup' );

// Theme Scripts & Styles
function elementor_hello_theme_scripts_styles() {
	wp_enqueue_style( 'elementor-hello-theme-style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'elementor_hello_theme_scripts_styles' );

// Register Elementor Locations
function elementor_hello_theme_register_elementor_locations( $elementor_theme_manager ) {
	$elementor_theme_manager->register_all_core_location();
};
add_action( 'elementor/theme/register_locations', 'elementor_hello_theme_register_elementor_locations' );




