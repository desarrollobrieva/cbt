<!DOCTYPE html>
<html lang="en" id="body-scroll">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126449654-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-126449654-2');
</script>
	<meta charset="UTF-8">
	<meta name="Title" content="astivik">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<meta name="description" content="">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/assets/img/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo get_template_directory_uri();?>/assets/img/favicon.ico" type="image/x-icon">
	<?php wp_head(); ?>
	<title>Astivik</title>
	<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1965651283511840');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1965651283511840&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body >
	<script>
  fbq('track', 'Lead');
</script>
<script>
  fbq('track', 'CompleteRegistration');
</script>

	<?php include('loader.php'); ?>
	<?php $currentlang = get_bloginfo('language'); ?>
	<div class="menu-principal">
		<div class="container">
			<section id="topbar" class=" d-lg-block">
				<div class="container clearfix content-sol">
					<div class="contact-info float-left">
						<button type="button" data-toggle="modal" data-target="#modalTime">
							<div class="sol">
								<img src="<?php echo get_template_directory_uri();?>/assets/img/cloud.png" alt="Temperatura">
							</div>
						</button>

					</div>
					<div class="social-links float-right">

						<!-- español-->
						<?php if($currentlang=="es-ES"):?>
							<!-- español-->
							<a href="?lang=en" class="instagram" style="vertical-align: middle;"><i class="" aria-hidden="true"></i><i class="sf">English</i></a>

							<!-- end español -->
						<?php else: ?>
							<!-- english -->
							<a href="?lang=es" class="instagram" style="vertical-align: middle;"><i class="t" aria-hidden="true"></i><i class="sf">Español</i></a>
							<!-- end English -->
						<?php endif; ?>


						<a href="#" class="instagram shared-border " role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<div class="icon-shared-orange"><img src="<?php echo get_template_directory_uri();?>/assets/img/compartir.svg" alt=""></div>
						</a>
						<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
							<?php if (is_front_page()): ?>
								<a class="dropdown-item item-shared-nav" href="https://www.facebook.com/sharer/sharer.php?u=<?php bloginfo('url'); ?>" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
								<a class="dropdown-item item-shared-nav" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php bloginfo('url'); ?>" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a>
							<a class="dropdown-item item-shared-nav" href="https://www.instagram.com/astivikshipyard/" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
								<a class="dropdown-item item-shared-nav" href="https://api.whatsapp.com/send?phone=573145966856" target="_blank"><i class="fa fa-whatsapp fa-2x"></i></a>

							<?php elseif (is_tax('Servicios')): ?>
								<a class="dropdown-item item-shared-nav" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo bloginfo('url').'/index.php/servicios/'.basename($_SERVER['PHP_SELF']); ?>" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
								<a class="dropdown-item item-shared-nav" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo bloginfo('url').'/index.php/servicios/'.basename($_SERVER['PHP_SELF']); ?>" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a>
								<a class="dropdown-item item-shared-nav" href="https://www.instagram.com/astivikshipyard/" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
								<a class="dropdown-item item-shared-nav" href="https://api.whatsapp.com/send?phone=573145966856" target="_blank"><i class="fa fa-whatsapp fa-2x"></i></a>

							<?php elseif (is_tax('tipo_proyectos')): ?>
								<a class="dropdown-item item-shared-nav" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo bloginfo('url').'/index.php/tipo_proyectos/'.basename($_SERVER['PHP_SELF']); ?>" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
								<a class="dropdown-item item-shared-nav" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo bloginfo('url').'/index.php/tipo_proyectos/'.basename($_SERVER['PHP_SELF']); ?>" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a>
								<a class="dropdown-item item-shared-nav" href="https://www.instagram.com/astivikshipyard/" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
								<a class="dropdown-item item-shared-nav" href="https://api.whatsapp.com/send?phone=573145966856" target="_blank"><i class="fa fa-whatsapp fa-2x"></i></a>

							<?php else: ?>
								<a class="dropdown-item item-shared-nav" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-facebook fa-2x"></i></a>
								<a class="dropdown-item item-shared-nav" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a>
								<?php if (have_posts()) : while( have_posts() ) : the_post(); ?>


									<a class="dropdown-item item-shared-nav" href="https://www.instagram.com/astivikshipyard/" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
								<a class="dropdown-item item-shared-nav" href="https://api.whatsapp.com/send?phone=573145966856" target="_blank"><i class="fa fa-whatsapp fa-2x"></i></a>

								<?php endwhile; endif; ?>

							<?php  endif;?>
						</div>
						<?php if($currentlang=="es-ES"):?>
							<!-- Español-->
							<a href="<?php echo bloginfo('url').'/index.php/ayuda#tab4'; ?>" class="facebook" style="vertical-align: middle;"><i class="sf">Proveedores</i></a>
							<a class="emergencia" href="http://clientes.astivik.com/users/sign_in" class="twitter" target="_blank"><i class="fa fa-shi">


								<button type="submit" class="btn  cliente-boton  fa fa-ship" ><span class="item-button">cliente astivik</span></button></i>
							</a>

							<!-- End Español -->
						<?php else: ?>
							<!-- English -->
							<a href="<?php echo bloginfo('url').'/index.php/ayuda#tab4'; ?>" class="facebook" style="vertical-align: middle;"><i class="sf">Suppliers</i></a>
							<a class="emergencia" href="http://clientes.astivik.com/users/sign_in" class="twitter" target="_blank"><i class="fa fa-shi">


								<button type="submit" class="btn  cliente-boton  fa fa-ship" ><span class="item-button">Astivik Client</span></button></i>
							</a>

							<!-- End English -->
						<?php endif; ?>

					</div>
				</div>
			</section>
			<header id="header">
				<div class="container">

					<div id="logo" class="pull-left">
						<a href="<?php echo bloginfo('url').'/index.php'; ?>"><img src="<?php echo get_template_directory_uri();?>/assets/img/logo_astivik.svg" alt="" title="" /></a>
					</div>
					<nav id="nav-menu-container ">
						<ul class=" nav-menu-firt">

							<?php if($currentlang=="es-ES"):?>
								<!-- Español-->
								<li class="menu-active"><a href="<?php echo bloginfo('url').'/index.php/servicio/'; ?>">Servicios</a></li>
								<li><a href="<?php echo bloginfo('url').'/index.php/proyecto'; ?>">Proyectos realizados</a></li>
								<li><a href="<?php echo bloginfo('url').'/index.php/mapa-infraestructura/'; ?>">Instalaciones</a></li>
								<?php $menu ='MENÚ' ?>
								<!-- End Español -->
							<?php else: ?>
								<!-- English -->
								<li class="menu-active"><a href="<?php echo bloginfo('url').'/index.php/servicio/'; ?>">Services</a></li>
								<li><a href="<?php echo bloginfo('url').'/index.php/proyecto'; ?>">Successful Projects</a></li>
								<li><a href="<?php echo bloginfo('url').'/index.php/mapa-infraestructura/'; ?>">Facilities</a></li>
								<!-- End English -->
								<?php $menu ='MENU' ?>
							<?php endif; ?>




							<div class="button-burger" id="burguer-desktop">
								<li style="border: 1px solid #0051db;background: #fff; color:#0051db; "><a><?php echo $menu; ?>
									<div class="hamburger hamburger--squeeze" id="squeeze">
										<div class="hamburger-box">
											<div class="hamburger-inner"></div>
										</div>
									</div>
								</div>
							</a>
						</li>

					</ul>
					<div class="button-burger  d-sm-block d-md-none" id="burguer-xs">
						<div class="hamburger hamburger--squeeze" id="squeeze_xs">
							<div class="hamburger-box">
								<div class="hamburger-inner"></div>
							</div>
						</div>
					</div>
					<nav class="menu" id="menu">
						<div class="frame-menu"> <!-- Desktop frame menu-->
							<div class="container">
								<div class="row">
									<div class=" col-6  ">
										<div class="logo-frame">
											<img src="<?php echo get_template_directory_uri();?>/assets/img/logo_astivik.svg" alt="">
										</div>
										<style>
										.logo-frame{width:200px;height:50px;margin-bottom:50px}.logo-frame img{max-height:100%}.icons-social-navbar{float:right;padding-right:50px}.icons-social-navbar li{margin:0 11px}.icons-social-navbar li a i{color:#000;cursor:pointer;font-size:1.5em}
									</style>
								</div>
								<div class="col-6">
									<div class="icons-social-navbar">
										<ul style="display: inline-flex;">
											<li> <a href="https://www.facebook.com/ASTIVIK"> <i class="fa fa-facebook fa-2x "></i></a>  </li>
											<li><a  target="_blank" href="https://www.instagram.com/astivikshipyard"> <i class="fa fa-instagram fa-2x "></i></a> </li>
											<li><a  target="_blank" href="https://www.linkedin.com/company/industrias-astivik"> <i class="fa fa-linkedin fa-2x "></i></a></span></li>
											<li><a  target="_blank" href="https://www.youtube.com/user/Astivik1"> <i class="fa fa-youtube-play fa-2x "></i></a></span></li>
											<li><a href="https://api.whatsapp.com/send?phone=573145966856" target="_blank"><i class="fa fa-whatsapp fa-2x"></i></a></li>
										</ul>
									</div>
								</div>
								<ul class="col-lg-3 col-sm-auto col-sm-12">

									<?php if($currentlang=="es-ES"):?>
										<!-- Español-->
										<li class="proyecto-menu">Acerca de Astivik</li>
										<li><a href="<?php echo bloginfo('url').'/index.php/about/#quienes-somos'; ?>">Quiénes somos</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/about/#mision'; ?>">Proyección y misión</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/about/#valores'; ?>">Valores de Astivik</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/seguridad/'; ?>">Control y medio ambiente</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/seguridad/'; ?>">Seguridad y responsabilidad</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/politica-para-el-tratamiento-de-datos-personales'; ?>">Políticas de Privacidad</a></li>
										<?php $service = 'Servicios';
										$contact = 'Contacto y Soporte';
										$capacity = 'Capacidades'; 
										$acerca = 'Acerca de Astivik'; 
										$project = 'Proyectos';?>
										<!-- End Español -->
									<?php else: ?>
										<!-- English -->
										<li class="proyecto-menu">About Astivik</li>
										<li><a href="<?php echo bloginfo('url').'/index.php/about/#quienes-somos'; ?>">About Us</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/about/#mision'; ?>">Mission and Vision</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/about/#valores'; ?>">Our Values</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/seguridad/'; ?>">Control and Environment</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/seguridad/'; ?>">Safety and Responsibility</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/politica-para-el-tratamiento-de-datos-personales'; ?>">Privacy Policies</a></li>
										<?php $service = 'Servicios';
										$contact = 'Contacto y Soporte';
										$capacity = 'Capacidades';
										$acerca = 'Acerca de Astivik'; 
										$project = 'Projects';?>
										<!-- End English -->
									<?php endif; ?>



								</ul>
								<ul class="col-lg-3 col-sm-auto col-sm-12">
									<li class="proyecto-menu"><?php echo $service; ?></li>
									<?php $categories = get_categories( array('taxonomy' => 'Servicios','hide_empty' => false,'orderby' => 'name','order' => 'ASC'));?>
									<?php foreach ($categories as $category):?>
										<li><a href="<?php echo get_term_link( $category->term_id ); ?>"><?php echo $category->name; ?></a></li>
									<?php endforeach; ?>

								</ul>
								<ul class="col-lg-3 col-sm-auto col-sm-12">
									<li class="proyecto-menu"><?php echo $capacity.' Astivik'; ?></li>
									<?php $the_query = new WP_Query('post_type=capacidades&order=ASC'); ?>
									<?php if ($the_query -> have_posts()) : while( $the_query -> have_posts() ) : $the_query -> the_post(); ?>

										<li><a href="<?php the_permalink();?>"><?php the_title(); ?></a></li>
									<?php endwhile; endif; ?>

									<?php if($currentlang=="es-ES"):?>
										<!-- Español-->
										<li><a href="<?php echo bloginfo('url').'/index.php/mapa-infraestructura/'; ?>">Mapa de Infraestructura</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/'; ?>">Programar cita</a></li>
										<!-- End Español -->
									<?php else: ?>
										<!-- English -->
										<li><a href="<?php echo bloginfo('url').'/index.php/mapa-infraestructura/'; ?>">Map of Infrastructure</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/'; ?>">Schedule Appointment</a></li>
										<!-- End English -->
									<?php endif; ?>


								</ul>
								<ul class="col-lg-3 col-sm-auto col-sm-12">

									<?php if($currentlang=="es-ES"):?>
										<!-- Español-->
										<li class="proyecto-menu">Contacto y Soporte</li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/#tab5'; ?>">Directorio corporativo</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/#tab4'; ?>">Proveedores</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab1'; ?>">Cotizar Servicios</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab7'; ?>">PQRS</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/#tab3'; ?>">Hablar con un asesor</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab6'; ?>">Preguntas Frecuentes</a></li>
										<!-- End Español -->
									<?php else: ?>
										<!-- English -->
										<li class="proyecto-menu">Contact and Support</li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/#tab5'; ?>">Corporate Directory</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/#tab4'; ?>">Suppliers</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab1'; ?>">Quote Service</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab7'; ?>">Quality Survey</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/#tab3'; ?>">Talk to an Agent</a></li>
										<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab6'; ?>">FAQ</a></li>
										<!-- End English -->
									<?php endif; ?>

								</ul>
								<ul class="col-lg-3 color-negro col-sm-auto col-sm-12">

									<?php if($currentlang=="es-ES"):?>
										<!-- Español-->
										<li class="color-negro"><a href="<?php echo bloginfo('url').'/index.php/proyecto/'; ?>">Proyectos Realizados</a></li>
										<li class="proyecto-menu"><a href="<?php echo bloginfo('url').'/index.php/about/noticias'; ?>">Noticias y Eventos</a></li>

										<li class="proyecto-menu"><a href="<?php echo bloginfo('url').'/index.php/about/#quienes-somos'; ?>">Quienes somos</a></li>
										<!-- End Español -->
									<?php else: ?>
										<!-- English -->
										<li class="color-negro"><a href="<?php echo bloginfo('url').'/index.php/proyecto/'; ?>">Succesful Projects</a></li>
										<li class="proyecto-menu"><a href="<?php echo bloginfo('url').'/index.php/about/noticias'; ?>">News and Events</a></li>

										<li class="proyecto-menu"><a href="<?php echo bloginfo('url').'/index.php/about/#quienes-somos'; ?>">About Us</a></li>
										<!-- End English -->
									<?php endif; ?>


									<div class="img-menu">
										<img style="        width: 270px;
										margin: 48px 27px 0px;;position: absolute; " src="<?php echo get_template_directory_uri();?>/assets/img/detalle-capacidad/03_background_ship.png" alt="">
									</div>
								</ul>

							</div>
						</div>
					</div>
				</nav>
				<nav class="menu" id="menu_xs">
					<!-- xs********************************************************************************************************************** -->
					<div class="frame-menu_xs"> <!-- Desktop frame menu-->
						<div class="container">
							<div class="row">
								<div class="img-menu-logo col-md-12 col-sm-auto  ">
									<img src="<?php echo get_template_directory_uri();?>/assets/img/logo_astivik.svg" alt="">
								</div>
							</div>
							<div class="menu-accordion">
								<div id="accordion">
									<div class="card">
										<div class="card-header" id="headingOne">
											<h5 class="mb-0">

												<!-- español-->
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
													<a class="proyecto-menu"><?php echo $acerca; ?></a>
												</button>
												<!-- end español -->

											</h5>
										</div>

										<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
											<div class="card-body">
												<ul>

													<?php if($currentlang=="es-ES"):?>
														<!-- Español-->
														<li><a href="<?php echo bloginfo('url').'/index.php/about/#quienes-somos'; ?>">Quiénes somos</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/about/#mision'; ?>">Proyección y misión</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/about/#valores'; ?>">Valores de Astivik</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/seguridad/'; ?>">Control y medio ambiente</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/seguridad/'; ?>">Seguridad y responsabilidad</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/politica-para-el-tratamiento-de-datos-personales'; ?>">Politicas de Privacidad</a></li>
														<!-- End Español -->
													<?php else: ?>
														<!-- English -->
														<li><a href="<?php echo bloginfo('url').'/index.php/about/#quienes-somos'; ?>">About Us</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/about/#mision'; ?>">Mission and Vision</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/about/#valores'; ?>">Our Values</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/seguridad/'; ?>">Control and Environment</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/seguridad/'; ?>">Safety and Responsibility</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/politica-para-el-tratamiento-de-datos-personales'; ?>">Privacy Policy</a></li>
														<!-- End English -->
													<?php endif; ?>

												</ul>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="headingTwo">
											<h5 class="mb-0">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
													<a class="proyecto-menu"><?php echo $service; ?></a>
												</button>
											</h5>
										</div>
										<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
											<div class="card-body">
												<ul>
													<?php $categories = get_categories( array('taxonomy' => 'Servicios','hide_empty' => false,'orderby' => 'name','order' => 'DES'));?>
													<?php foreach ($categories as $category):?>
														<li><a href="<?php echo get_term_link( $category->term_id ); ?>"><?php echo $category->name; ?></a></li>
													<?php endforeach; ?>
												</ul>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="headingThree">
											<h5 class="mb-0">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
													<a class="proyecto-menu"><?php echo $capacity.' Astivik'; ?></a>
												</button>
											</h5>
										</div>
										<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
											<div class="card-body">
												<ul>
													<?php $the_query = new WP_Query('post_type=capacidades&order=ASC'); ?>
													<?php if ($the_query -> have_posts()) : while( $the_query -> have_posts() ) : $the_query -> the_post(); ?>

														<li><a href="<?php the_permalink();?>"><?php the_title(); ?></a></li>
													<?php endwhile; endif; ?>
													<?php if($currentlang=="es-ES"):?>
														<!-- español-->
														<li><a href="<?php echo bloginfo('url').'/index.php/mapa-infraestructura/'; ?>">Mapa de Infraestructura</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/'; ?>">Programar cita</a></li>
														<!-- end español -->
													<?php else: ?>
														<!-- english -->
														<li><a href="<?php echo bloginfo('url').'/index.php/mapa-infraestructura/'; ?>">Infrastructure Map</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda/'; ?>">Schedule Appointment</a></li>
														<!-- end English -->
													<?php endif; ?>

												</ul>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="headingFour">
											<h5 class="mb-0">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
													<a class="proyecto-menu"><?php echo $contact; ?></a>
												</button>
											</h5>
										</div>
										<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
											<div class="card-body">
												<ul>
													<?php if($currentlang=="es-ES"):?>
														<!-- español-->
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab5'; ?>">Directorio corporativo</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab4'; ?>">Proveedores</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab1'; ?>">Cotizar Servicios</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab7'; ?>">PQRS</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab3'; ?>">Hablar con un asesor</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab6'; ?>">Preguntas Frecuentes</a></li>
														<!-- end español -->
													<?php else: ?>
														<!-- english -->
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab5'; ?>">Corporate Directory</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab4'; ?>">Suppliers</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab1'; ?>">Quote services</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab7'; ?>">Quality Survey</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab3'; ?>">Talk to an Agent</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/ayuda#tab6'; ?>">FAQ</a></li>
														<!-- end English -->
													<?php endif; ?>
												</ul>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-header" id="headingFive">
											<h5 class="mb-0">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
													<a class="proyecto-menu"><?php echo $project; ?></a>
												</button>
											</h5>
										</div>
										<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
											<div class="card-body">
												<ul>
													<?php if($currentlang=="es-ES"):?>
														<!-- Español-->
														<li><a href="<?php echo bloginfo('url').'/index.php/proyecto'; ?>" title="">Proyectos Realizados</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/noticias'; ?>" title="">Noticias y Evento</a></li>
														
														<!-- End Español -->
													<?php else: ?>
														<!-- English -->
														<li><a href="<?php echo bloginfo('url').'/index.php/proyecto'; ?>" title="">Successful Projects</a></li>
														<li><a href="<?php echo bloginfo('url').'/index.php/noticias'; ?>" title="">News and Events</a></li>
														<!-- End English -->
													<?php endif; ?>
												</ul>
											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</nav>
			</li>
		</nav>
	</div>
</header>
</div>
</nav>
</ul>
</nav>
</div>
</header>
</div>
</div>
</div>
<section class="content"><!--  starconten -->
	<!-- Modal Time -->
	<div class="modal fade" id="modalTime" tabindefx="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-dialog-time" role="document">
			<div class="modal-content">
				<div class="modal-body">

					<!-- español-->
						<?php if($currentlang=="es-ES"):?>
							<!-- español-->

							<!-- weather widget start --><div id="m-booked-bl-simple-23793"> <div class="booked-wzs-160-110 weather-customize" style="background-color:#137AE9;width:250px;" id="width3"> <div class="booked-wzs-160-110_in"> <a target="_blank" class="booked-wzs-top-160-110" href="https://www.booked.net/"><img src="//s.bookcdn.com/images/letter/s5.gif" alt="https://www.booked.net/" /></a> <div class="booked-wzs-160-data"> <div class="booked-wzs-160-left-img wrz-03"></div> <div class="booked-wzs-160-right"> <div class="booked-wzs-day-deck"> <div class="booked-wzs-day-val"> <div class="booked-wzs-day-number"><span class="plus">+</span>28</div> <div class="booked-wzs-day-dergee"> <div class="booked-wzs-day-dergee-val">&deg;</div> <div class="booked-wzs-day-dergee-name">C</div> </div> </div> <div class="booked-wzs-day"> <div class="booked-wzs-day-d"><span class="plus">+</span>28&deg;</div> <div class="booked-wzs-day-n"><span class="plus">+</span>28&deg;</div> </div> </div> <div class="booked-wzs-160-info"> <div class="booked-wzs-160-city smolest">Cartagena de Indias</div> <div class="booked-wzs-160-date">Lunes, 15</div> </div> </div> </div> <a target="_blank" href="https://hotelmix.es/weather/cartagena-w10353" class="booked-wzs-bottom-160-110"> <div class="booked-wzs-center"><span class="booked-wzs-bottom-l"> Previsión para 7 días</span></div> </a> </div> </div> </div><script type="text/javascript"> var css_file=document.createElement("link"); css_file.setAttribute("rel","stylesheet"); css_file.setAttribute("type","text/css"); css_file.setAttribute("href",'https://s.bookcdn.com/css/w/booked-wzs-widget-160.css?v=0.0.1'); document.getElementsByTagName("head")[0].appendChild(css_file); function setWidgetData(data) { if(typeof(data) != 'undefined' && data.results.length > 0) { for(var i = 0; i < data.results.length; ++i) { var objMainBlock = document.getElementById('m-booked-bl-simple-23793'); if(objMainBlock !== null) { var copyBlock = document.getElementById('m-bookew-weather-copy-'+data.results[i].widget_type); objMainBlock.innerHTML = data.results[i].html_code; if(copyBlock !== null) objMainBlock.appendChild(copyBlock); } } } else { alert('data=undefined||data.results is empty'); } } </script> <script type="text/javascript" charset="UTF-8" src="https://widgets.booked.net/weather/info?action=get_weather_info&ver=6&cityID=w10353&type=1&scode=124&ltid=3457&domid=582&anc_id=53003&cmetric=1&wlangID=4&color=137AE9&wwidth=250&header_color=ffffff&text_color=333333&link_color=08488D&border_form=1&footer_color=ffffff&footer_text_color=333333&transparent=0"></script><!-- weather widget end -->
							

							<!-- end español -->
						<?php else: ?>
							<!-- english -->
						<!-- weather widget start --><div id="m-booked-bl-simple-9932"> <div class="booked-wzs-160-110 weather-customize" style="background-color:#137AE9;width:250px;" id="width3"> <div class="booked-wzs-160-110_in"> <a target="_blank" class="booked-wzs-top-160-110" href="https://www.booked.net/"><img src="//s.bookcdn.com/images/letter/s5.gif" alt="https://www.booked.net/" /></a> <div class="booked-wzs-160-data"> <div class="booked-wzs-160-left-img wrz-03"></div> <div class="booked-wzs-160-right"> <div class="booked-wzs-day-deck"> <div class="booked-wzs-day-val"> <div class="booked-wzs-day-number"><span class="plus">+</span>28</div> <div class="booked-wzs-day-dergee"> <div class="booked-wzs-day-dergee-val">&deg;</div> <div class="booked-wzs-day-dergee-name">C</div> </div> </div> <div class="booked-wzs-day"> <div class="booked-wzs-day-d"><span class="plus">+</span>28&deg;</div> <div class="booked-wzs-day-n"><span class="plus">+</span>28&deg;</div> </div> </div> <div class="booked-wzs-160-info"> <div class="booked-wzs-160-city">Cartagena</div> <div class="booked-wzs-160-date">Monday, 15</div> </div> </div> </div> <a target="_blank" href="https://www.booked.net/weather/cartagena-w10353" class="booked-wzs-bottom-160-110"> <div class="booked-wzs-center"><span class="booked-wzs-bottom-l"> See 7-Day Forecast</span></div> </a> </div> </div> </div><script type="text/javascript"> var css_file=document.createElement("link"); css_file.setAttribute("rel","stylesheet"); css_file.setAttribute("type","text/css"); css_file.setAttribute("href",'https://s.bookcdn.com/css/w/booked-wzs-widget-160.css?v=0.0.1'); document.getElementsByTagName("head")[0].appendChild(css_file); function setWidgetData(data) { if(typeof(data) != 'undefined' && data.results.length > 0) { for(var i = 0; i < data.results.length; ++i) { var objMainBlock = document.getElementById('m-booked-bl-simple-9932'); if(objMainBlock !== null) { var copyBlock = document.getElementById('m-bookew-weather-copy-'+data.results[i].widget_type); objMainBlock.innerHTML = data.results[i].html_code; if(copyBlock !== null) objMainBlock.appendChild(copyBlock); } } } else { alert('data=undefined||data.results is empty'); } } </script> <script type="text/javascript" charset="UTF-8" src="https://widgets.booked.net/weather/info?action=get_weather_info&ver=6&cityID=w10353&type=1&scode=124&ltid=3457&domid=&anc_id=16609&cmetric=1&wlangID=1&color=137AE9&wwidth=250&header_color=ffffff&text_color=333333&link_color=08488D&border_form=1&footer_color=ffffff&footer_text_color=333333&transparent=0"></script><!-- weather widget end -->
							<!-- end English -->
						<?php endif; ?>

				</div>
			</div>
		</div>
	</div>


	<style type="text/css">
	.modal-dialog-time{max-width:335px!important}.active-scroll{overflow:hidden!important}
</style>
<section class="content"><!--  starconten -->