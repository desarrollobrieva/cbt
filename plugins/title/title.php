<?php
/*
* Plugin Name: Curt title plugin
* Plugin URI: https://www.github.com/oxisrock
* Description: Titulo y excerpt cortando
* Version: 1.1
* Author: Francisco Aular
* Author URI: https://www.github.com/oxisrock
* License: GPL2
*/

add_filter( 'the_content', 'frantitle_title');

function frantitle_title($title , $limit=1, $trailing='...',$break=”.”) {
  if (!is_single() && !is_page() ) {
  $title = get_the_title();
  $title = strip_tags($title);
  $words = explode(' ',$title);
  if ( count($words) >= $limit) {
    $title = implode(' ', array_slice($words, 0, $limit)). $trailing;
    return($title);
  }
  else {
    return($title);
  }
}
else {
  return($title);
}
}

?>
